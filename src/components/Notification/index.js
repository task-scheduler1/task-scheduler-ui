import React, { Component } from "react";
import API, { API_SERVER } from '../../repository/api';
import Storage from '../../repository/storage';
import moment from 'moment-timezone';
import SocketContext from '../../socket';
import { withTranslation } from "react-i18next";

class NotificationClass extends Component {

  constructor(props) {
    super(props);
    this.state = {
      notificationData: [],
      tabIndex: 1,
      badgeNotif: 0,
      badgeRemind: 0,
      filterType: '',
      filterNotification: '',
      notif: [],
      notifFilter: []
    };
  }

  filterNotification = (e) => {
    e.preventDefault();
    this.setState({ filterNotification: e.target.value });
  }

  changeFilterType = e => {
    this.setState({ filterType: e.target.value })
  }


  componentDidMount() {
    this.fetchNotif();

  }

  deleteNotif = e => {
    e.preventDefault();
    API.delete(`${API_SERVER}v1/notification/id/${e.target.getAttribute('data-id')}`).then(res => {
      if (res.data.error) console.log(`Error delete`)

      this.fetchNotif();
    })
  }

  deleteAllNotif = e => {
    e.preventDefault();
    let temp = [];
    this.state.notificationData.filter(item => {
      if (item.tag == 1) {
        temp.push(item.id)
      }
    });
    API.put(`${API_SERVER}v1/notification/id/${Storage.get('user').userId}`, { notifIds: temp }).then(res => {
      if (res.data.error) console.log(`Error delete`)

      this.fetchNotif();
    })
  }

  readAllNotif = e => {
    e.preventDefault();
    let temp = [];
    this.state.notificationData.filter(item => {
      if (item.tag == 1 && item.isread == 0) {
        temp.push(item.id)
      }
    });
    API.put(`${API_SERVER}v1/notification/read/all`, { userId: Storage.get('user').userId, notifIds: temp }).then(res => {
      if (res.data.error) console.log('Error update')

      this.props.socket.emit('send', { companyId: Storage.get('user').data.company_id })
      this.fetchNotif();
    })
  }

  fetchNotif() {
    API.get(`${API_SERVER}notifications`).then(res => {
      if (res.data.error) console.log('Gagal fetch unread')
      this.setState({ notificationData: res.data.result });
    })
  }

  readNotif(id) {
    // API.put(`${API_SERVER}v1/notification/read`, { id }).then(res => {
    //   if (res.data.error) console.log('Gagal read')

    //   this.props.socket.emit('send', { companyId: Storage.get('user').data.company_id })
    //   this.fetchNotif();
    // })
  }

  fetchCheckAccess(company_id, param) {
    API.get(`${API_SERVER}v2/notification-alert/check-access`, { company_id, param }).then(res => {
      if (res.status === 200) {
        console.log(res.data.result, 'test')
        this.setState({ notif: res.data.result })
      }
    })
  }

  fetchNotification() {
    let url = '';
    let types = '';

    if (types === 1) {
      url = `${API_SERVER}v2/notif?user_id=${Storage.get('user').userId}&type=3&tag=1&types=1`
    } else if (types === 2) {
      url = `${API_SERVER}v2/notif?user_id=${Storage.get('user').userId}&type=3&tag=1&types=2`
    }
    API.get(url).then(res => {
      if (res.status === 200) {
        this.setState({ notifFilter: res.data.result });
      }
    })
  }


  render() {
    const { t } = this.props
    let { notificationData } = this.state;


    return (
      <div className="pcoded-main-container">
        <div className="pcoded-wrapper">
          <div className="pcoded-content">
            <div className="pcoded-inner-content">
              <div className="main-body">
                <div className="page-wrapper">

                  <div className="row">
                    <h3>{t('notification')}</h3>



                    <div className="col-sm-12" style={{ margin: '10px 10px 10px 0' }}>
                      <div className="row">
                        {/* <div className="col-sm-3">

                            <select value={this.state.filterType} onChange={this.changeFilterType} style={{ width: '100%', height: 40, border: '1px solid #ced4da', borderRadius: '.25rem', color: '#949ca6' }}>
                              <option value=''>All</option>
                              <option value='3'>Meeting</option>
                              <option value='4'>Announcement</option>
                              <option value='5'>Task</option>
                              <option value='6'>Files</option>
                            </select>
                          </div> */}
                        <div className="col-sm-3">
                          <input
                            type="text"
                            placeholder="Search"
                            onChange={this.filterNotification}
                            className="form-control" />
                        </div>
                        <div className="float-right">
                          <button href="" onClick={this.readAllNotif} className="btn btn-v2 btn-primary "> Read all</button>
                          <button href="" onClick={this.deleteAllNotif} className="btn btn-v2 btn-danger ml-3"> Remove all</button>
                        </div>
                      </div>
                      {notificationData.length === 0 ?
                        <div style={{ width: '-webkit-fill-available', marginTop: '15px', padding: 20 }}>
                          <b className="fc-blue ">No notifications at this time ...</b>
                        </div>
                        :
                        <span>
                          {
                            notificationData.map((item, i) => {
                              return (
                                <div onClick={() => this.readNotif(item.id)} className="row" key={item.id} style={{ cursor: 'pointer', background: '#FFF', borderRadius: 4, padding: '12px', margin: '10px 10px 10px -15px' }}>
                                  <span style={{ width: '-webkit-fill-available', position: 'relative' }}>
                                    {
                                      item.isActive === 0 &&
                                      <span style={{ margin: '5px', padding: '1px 6px', borderRadius: '8px', color: 'white', background: 'red' }}>new</span>
                                    }
                                    <b className="fc-blue ">
                                      {"Notifikasi"}
                                    </b>
                                    &nbsp; &nbsp;
                                    <small>
                                      {moment.utc(item.created_at).tz(moment.tz.guess(true)).format('HH:mm')} &nbsp;
                                      {moment.utc(item.created_at).tz(moment.tz.guess(true)).format('DD/MM/YYYY')}
                                    </small>
                                    <p className="fc-muted mt-1">
                                      {item.description}
                                    </p>

                                    <i className="fa fa-trash float-right" style={{ position: 'absolute', top: '0px', right: '0px', cursor: 'pointer' }} onClick={this.deleteNotif} data-id={item.id}></i>
                                  </span>

                                </div>
                              )
                            })
                          }
                        </span>
                      }
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


      </div>

    );
  }
}

const Notification = props => (
  <SocketContext.Consumer>
    {socket => <NotificationClass {...props} socket={socket} />}
  </SocketContext.Consumer>
)

const NotifWithTranslation = withTranslation('common')(Notification)

export default NotifWithTranslation;
