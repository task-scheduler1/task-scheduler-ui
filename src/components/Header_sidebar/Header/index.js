/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import API, { USER_ME, API_SERVER } from '../../../repository/api';
import Storage from '../../../repository/storage';
import moment from 'moment-timezone';

class Header extends Component {
  state = {
    user: '',
    level: '',
    avatar: '/assets/images/user/avatar-1.png',
    dataHeader: [],

    company: [],
    company_id: '',
    myCompanyName: '',

    menuAktif: '/',
    sideMenu: false,
    sideMenuCollapse: false,
    dateNow: Date.now(),
    logo: '',
    logoMulti: ''
  };


  // goTo = (id) => {
  //   let data = this.state.dataHeader.find((x) => x.id == id);
  //   if (typeof data == 'object') {
  //     API.get('v1/notification/read', { id: id }).then((res) => {
  //       if (data.destination) {
  //         window.location = data.destination;
  //       }
  //     });
  //   }
  // };

  mobileMenuClicked() {
    this.setState({ sideMenu: false });
  }

  async componentDidMount() {
    API.get(
      `${API_SERVER}users/dashboard`
    ).then((res) => {
      Storage.set('user_role', res.data.result);

      this.setState({ dataHeader: res.data.result });
    });
  }

  render() {
    const { dataHeader } = this.state;
    return (
      <header className="navbar pcoded-header navbar-expand-lg navbar-light" style={{ marginBottom: -1, background: '#FFF' }}>


        <div className="collapse navbar-collapse">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item dropdown">
              <Link to="/pengaturan">
                <div className="media">
                  <img
                    alt="Media"
                    className="img-radius"
                    style={{ width: 40, height: 40 }}
                    src="assets/images/user/avatar-2.jpg"
                  />
                  <div className="media-body mt-1 ml-1">
                    <h6 className="chat-header f-w-900">
                      Fikran Jabbar Pasya
                      <small
                        className="d-block  mt-2 text-c-grey"
                        style={{ textTransform: 'capitalize' }}
                      >
                        {dataHeader.fullname}
                      </small>
                    </h6>
                  </div>
                </div>
              </Link>
            </li>

          </ul>

          <ul className="navbar-nav ml-auto">

            <span className="fc-muted">{moment().local().format('DD MMMM YYYY')}</span>

          </ul>

          <ul className="navbar-nav">
            <li className="nav-item dropdown">
              <div className="media">
                <img
                  alt="Media"
                  style={{ height: 26 }}
                  src="newasset/task/logo-task.png"
                />
              </div>
            </li>
          </ul>
        </div>
      </header>
    );
  }
}

export default Header;
