/* eslint-disable array-callback-return */
import React, { Component } from "react";
import { Link } from "react-router-dom";
import Storage from '../../../repository/storage';
import Tooltip from '@material-ui/core/Tooltip';
import API, { API_SERVER } from '../../../repository/api';
import SocketContext from '../../../socket';
class SidebarClass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuAktif: '/',
      sideMenu: true,
      sideMenuCollapse: false,
      companyName: '',

      notifUnread: 0,

    };
  }

  componentDidMount() {
    this.setState({ companyName: 'TASK SCHEDULER' })
    this.setState({ menuAktif: window.location.pathname })

    this.fetchNotif()
  }


  fetchNotif() {
    API.get(`${API_SERVER}notifications/count`).then(res => {
      if (res.data.error) console.log('Gagal fetch unread')
      this.setState({ notifUnread: res.data.result });
    })
  }

  render() {
    // let access = Storage.get('access');
    // let levelUser = Storage.get('user').level;

    let menuSuperAdmins = {
      submenu: [

        { iconOn: 'files.svg', iconOff: 'files.svg', label: "Project", link: '/project' },
        { iconOn: 'info.svg', iconOff: 'info.svg', label: "User's Task Report", link: '/gantt/report' },


        { iconOn: 'user-sidebar.svg', iconOff: 'user-sidebar.svg', label: 'Users', link: '/user' },
        { iconOn: 'logout-sidebar.svg', iconOff: 'logout-sidebar.svg', label: 'Logout', link: '/logout' },
      ],
      menuAtas: [
        { iconOn: 'dashboard-on.svg', iconOff: 'dashboard.svg', label: 'Dashboard', link: '/' },
        { iconOn: 'calendar-on.svg', iconOff: 'calendar.svg', label: 'Calendar', link: '/calendar' },
        { iconOn: 'notification.svg', iconOff: 'notification.svg', label: 'Notification', link: '/notification', isBadge: true },
        // { iconOn: 'mail-2.svg', iconOff: 'mail-2.svg', label: 'Announcement', link: '/pengumuman' },
      ],
      menuBawah: [
        { iconOn: 'setting-on.svg', iconOff: 'setting.svg', label: 'Settings', link: '/pengaturan' },
        { iconOn: 'bantuan-on.svg', iconOff: 'bantuan-off.svg', label: 'Support', link: '/bantuan' },
      ]
    };

    const { menuAktif } = this.state;

    let menuContent = [];
    let menuAtas = [];
    let menuBawah = [];

    let tempAtasSuper = [], tempBawahSuper = [];

    // if (levelUser === 'superadmin') {
    tempAtasSuper = menuSuperAdmins.menuAtas;
    tempBawahSuper = menuSuperAdmins.menuBawah;


    // }

    // if (levelUser === 'superadmin') {
    menuContent = menuSuperAdmins.submenu;
    menuAtas = tempAtasSuper;
    menuBawah = tempBawahSuper;
    // }
    return (
      <nav className="pcoded-navbar navbar-collapsed"> {/** navbar-collapsed */}
        <div className="navbar-wrapper" style={{ borderTopRightRadius: '60px' }}>
          <div className="navbar-brand header-logo">
            <Link to="/" className="b-brand" style={{ width: '100%' }}>
              <img
                src='assets/images/logo-white.png'
                alt=""
                style={{ width: '90%', height: 'auto', paddingLeft: '5%' }}
              />
              {/* <span className="b-title">IDEKU</span> */}
            </Link>

            {/* <a style={{cursor:'pointer'}} className="mobile-menu" id="mobile-collapse" ><span /></a> */}

          </div>

          {/* scroll-div */}
          <div className="navbar-content" style={{ background: 'none' }}>
            <ul className="nav pcoded-inner-navbar">
              {/* <li className="nav-item pcoded-menu-caption">
                  <label />
                </li> */}

              <li id="mobile-collapse" data-username="Sample Page"
                className={`nav-item`}
                style={this.state.sideMenu ? { width: 59, cursor: 'pointer' } : { marginTop: 12, cursor: 'pointer' }}  >
                <Tooltip title="Menu" arrow placement="right">
                  <div className="nav-link"
                    style={this.state.sideMenu ? { padding: '7px 0px', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' } : { padding: "7px 20px" }}
                  >
                    <span className="pcoded-micon" style={this.state.sideMenu ? { marginRight: 0, padding: 0, width: 'auto' } : null}>
                      <img
                        src={`newasset/burger-menu.svg`}
                        alt=""
                        width={25}
                      ></img>
                    </span>
                  </div>
                </Tooltip>
              </li>

              {
                menuAtas.map((item, i) => {
                  // if (item.access === undefined || access[item.access]) {
                  return (
                    <li data-username="Sample Page"
                      className={`nav-item`}
                      style={this.state.sideMenu ? { width: 59, cursor: 'pointer' } : { marginTop: 12, cursor: 'pointer' }}  >
                      <Tooltip title={item.label} arrow placement="right">
                        <Link className="nav-link" to={item.link}
                          style={this.state.sideMenu ? { marginTop: 18, padding: 0, display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', width: 'auto' } : { padding: "7px 20px" }}
                        >
                          <span className="pcoded-micon" style={this.state.sideMenu ? { marginRight: 0 } : null}>
                            <img
                              src={`newasset/${menuAktif === item.link ? item.iconOn : item.iconOff}`}
                              alt=""
                              width={25}
                            ></img>
                          </span>
                          {
                            (this.state.notifUnread && item.hasOwnProperty('isBadge')) ? <span className="badge-notif" style={this.state.notifUnread > 9 ? { padding: '1px 3px' } : { padding: '1px 6px' }}>{this.state.notifUnread}</span> : ''
                          }
                        </Link>
                      </Tooltip>
                    </li>
                  )
                  // }
                })
              }

            </ul>
            <ul className="nav pcoded-inner-navbar" style={{ position: 'fixed', bottom: 35 }}>

              {
                menuBawah.map((item, i) => {
                  // if (item.access === undefined || access[item.access]) {
                  return (
                    <li data-username="Sample Page"
                      className={`nav-item `}
                      style={this.state.sideMenu ? { width: 59 } : { marginTop: 25 }}
                    >
                      <Tooltip title={item.label} arrow placement="right">
                        <Link to={item.link} className="nav-link"
                          style={this.state.sideMenu ? { padding: '7px 0px', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' } : { padding: "7px 20px" }}
                        >
                          <span className="pcoded-micon" style={this.state.sideMenu ? { marginRight: 0 } : null}>
                            <img
                              src={`newasset/${menuAktif === item.link ? item.iconOn : item.iconOff}`}
                              alt=""
                              width={25}
                            ></img>
                          </span>
                        </Link>
                      </Tooltip>
                    </li>
                  )
                  // }
                })
              }

            </ul>
          </div>
        </div>

        <div className="custom-side-bar">
          <h6 className="p-20 mt-5" style={{ textAlign: 'center' }}><strong>{localStorage.getItem("companyName") ? localStorage.getItem("companyName") : this.state.companyName}</strong></h6>
          <div>
            <div className="side-submenu-groupname">MENU</div>
            {
              menuContent.map((item, i) => {
                // if (item.access === undefined || access[item.access]) {
                return (
                  <div>
                    {item.label === 'Logout' && <div className="side-submenu-groupname">SESSION</div>}
                    <Link to={item.link} style={{ color: '#797979' }}>
                      <div className="side-submenu">
                        <img
                          src={`newasset/${menuAktif === item.link ? item.iconOn : item.iconOff}`}
                          style={{ marginRight: 15 }}
                          alt=""
                          height={15}
                          width={15}
                        ></img>
                        {item.label}
                      </div>
                    </Link>
                  </div>
                )
                // }
              })
            }
          </div>
        </div>
      </nav>
    );
  }
}

const Sidebar = props => (
  <SocketContext.Consumer>
    {socket => <SidebarClass {...props} socket={socket} />}
  </SocketContext.Consumer>
)

export default Sidebar;
