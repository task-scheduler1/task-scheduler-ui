import React, { Component } from "react";
import { withTranslation } from 'react-i18next';

import { Link } from "react-router-dom";
import { Card } from 'react-bootstrap';
import API, { USER_ME, API_SERVER } from '../../repository/api';
import Storage from '../../repository/storage';


import { dataEvent } from '../../modul/data';
import CalenderNew from '../kalender/kalender';
// import ListToDoNew from './listToDo';
import RecentDocs from './recentDocs';
import JadwalHariIni from './jadwalHariIni';
import Files from './files';
import ChartVertical from './chartVertical';


import 'react-toastify/dist/ReactToastify.css';
import Tooltip from '@material-ui/core/Tooltip';

import { connect } from 'react-redux';
import { initUser } from '../../actions/user_action';

class HomeNew extends Component {
  state = {
    user: {
      name: 'Anonymous',
      registered: '2019-12-09',
      companyId: '',
    },
    event: [],
    project: [],
    findCourseInput: "",
    kategoriKursus: [],
    kursusTerbaru: [],
    kursusDiikuti: [],
    recentDocs: [],
    dataEvent: 'Dashboard'

  }

  tabsDahsboard(item) {
    console.log('apaan nih', item)
    this.setState({
      dataEvent: item
    })
  }

  onChangeInput = e => {
    const name = e.target.name;
    const value = e.target.value;

    if (name === 'attachmentId') {
      this.setState({ [name]: e.target.files });
    } else {
      this.setState({ [name]: value });
    }
  }

  componentDidMount() {
    // this.props.initUser();
    // this.fetchDataUser();
    // this.fetchDataKursusDiikuti();
    this.fetchEvent();


  }

  fetchDataUser() {
    API.get(`${USER_ME}${Storage.get('user').data.email}`).then(res => {
      if (res.status === 200) {

        this.fetchDataKategoriKursus(
          localStorage.getItem('companyID') ? localStorage.getItem('companyID') : res.data.result.company_id
        );
        this.fetchDataKursusTerbaru(
          localStorage.getItem('companyID') ? localStorage.getItem('companyID') : res.data.result.company_id
        );

        Object.keys(res.data.result).map((key, index) => {
          if (key === 'registered') {
            return res.data.result[key] = res.data.result[key].toString().substring(0, 10);
          }
        });

        this.setState({ user: res.data.result });
      }
    })
  }

  fetchDataKategoriKursus(companyId) {
    API.get(`${API_SERVER}v1/category/company/${localStorage.getItem('companyID') ? localStorage.getItem('companyID') : companyId}`).then(res => {
      if (res.status === 200) {
        this.setState({ kategoriKursus: res.data.result.filter(item => { return item.count_course > 0 }) })
      }
    })
  }

  fetchDataKursusTerbaru(companyId) {
    API.get(`${API_SERVER}v1/course/company/${localStorage.getItem('companyID') ? localStorage.getItem('companyID') : companyId}`).then(res => {
      if (res.status === 200) {
        this.setState({ kursusTerbaru: res.data.result.filter(item => { return item.count_chapter > 0 }).slice(0, 3) })
      }
    })
  }

  fetchDataKursusDiikuti() {
    API.get(`${API_SERVER}v1/user-course/${Storage.get('user').userId}`).then(res => {
      if (res.status === 200) {
        this.setState({ kursusDiikuti: res.data.result.reverse().slice(0, 6) })
      }
    })
  }

  fetchEvent() {
    this.setState({ event: dataEvent });
    // API.get(`${USER_ME}${Storage.get('user').data.email}`).then(res => {
    // console.log('apaan nih', res.data);
    // if (res.status === 200) {
    //   this.setState({ companyId: localStorage.getItem('companyID') ? localStorage.getItem('companyID') : res.data.result.company_id });
    //   API.get(`${API_SERVER}v1/event/${Storage.get('user').level}/${Storage.get('user').userId}/${this.state.companyId}`).then(response => {
    //     console.log('data EVENT', JSON.stringify(response.data.result))
    //     // recent docs
    //     API.get(`${API_SERVER}v1/files-logs/${this.state.companyId}`).then(res => {
    //       if (!res.data.error) {
    //         this.setState({ recentDocs: res.data.result });
    //       };
    //     });
    //   }).catch(function (error) {
    //     console.log(error);
    //   });
    // }
    // })
  }

  findCourse = (e) => {
    e.preventDefault();
    this.setState({ findCourseInput: e.target.value });
  }

  render() {
    const { t } = this.props;

    const eventDashboard = this.state.event;

    var { kategoriKursus, kursusTerbaru, kursusDiikuti, findCourseInput } = this.state;
    if (findCourseInput !== "") {
      [kategoriKursus, kursusTerbaru, kursusDiikuti] = [kategoriKursus, kursusTerbaru, kursusDiikuti]
        .map(y =>
          y.filter(x =>
            JSON.stringify(
              Object.values(x)
            ).replace(
              /[^\w ]/g, ''
            ).match(new RegExp(findCourseInput, "gmi"))
          )
        );
    }


    return (
      <div className="pcoded-main-container" style={{ backgroundColor: "#F6F6FD" }}>
        <div className="pcoded-wrapper">
          <div className="pcoded-content">
            <div className="pcoded-inner-content">
              <div className="main-body">
                <div className="page-wrapper">

                  <div className="row">
                    <div className="col-sm-12">
                      <Card>
                        <Card.Body>

                          <div className="row">
                            {
                              eventDashboard.length === 0 ?
                                <div className="col-sm-6 ">
                                  There is no available event
                                </div>
                                :
                                eventDashboard.map((item, i) => {

                                  return (
                                    <div className="col-sm-3" key={item.course_id} style={{ cursor: 'pointer' }} onClick={this.tabsDahsboard.bind(this, item.text)}>
                                      <div className={`box-event-${item.title.toLowerCase()}`}>
                                        {item.status === false && <Tooltip title="This event is not available" arrow placement="top"><div className="event-disabled" /></Tooltip>}
                                        <div className="box-event ">
                                          <div className="d-flex justify-content-center">
                                            <img
                                              src={`newasset/${item.title.toLowerCase()}-new.svg`}
                                              alt=""
                                              height={50}
                                            ></img>
                                          </div>
                                          <div className="d-flex justify-content-center">
                                            <div className={`title-head f-w-900 f-16 color-event-${item.title.toLowerCase()}`} style={{ lineHeight: '42px' }}>
                                              {/* {t(item.text.toLowerCase())} */}{item.text.toLowerCase()}
                                            </div>
                                          </div>
                                          <small className={`float-right color-event-${item.title.toLowerCase()} ${item.title === 'Meeting' || item.title === 'Webinar' ? 'hidden' : ''}`} style={{ fontSize: '16px', paddingRight: 14 }}>{item.total}</small>
                                        </div>
                                      </div>

                                    </div>
                                  )
                                })
                            }
                          </div>                        </Card.Body>
                      </Card>
                    </div>
                    <div className={`col-sm-12 col-xl-6 ${this.state.dataEvent === 'Dashboard' ? '' : 'hidden'}`} style={{ paddingLeft: 0, paddingRight: 0 }}>

                      <div className="col-sm-12">
                        <Card>
                          <Card.Body>
                            <div className="row">
                              <div className="col-sm-12">
                                <h3 className="f-w-900 f-18 fc-blue">
                                  {t('recently_accessed_documents')}
                                </h3>
                              </div>
                              <div className="col-sm-6 text-right">
                                <p className="m-b-0">
                                  {/* <span className="f-w-600 f-16">Lihat Semua</span> */}
                                </p>
                              </div>
                            </div>
                            <div className="wrap" style={{ overflowX: 'auto', overflowY: 'scroll', height: '400px' }}>

                              <RecentDocs lists={this.state.recentDocs} />

                            </div>
                          </Card.Body>
                        </Card>
                      </div>
                    </div>
                    <div className={`col-sm-12 col-xl-6 ${this.state.dataEvent === 'Dashboard' ? '' : 'hidden'}`} style={{ paddingLeft: 0, paddingRight: 0 }}>
                      <div className="col-sm-12">
                        <Card>
                          <Card.Body>
                            <div className="row">
                              <div className="col-sm-12">
                                <h3 className="f-w-900 f-18 fc-blue">
                                  Chart
                                </h3>
                              </div>
                              <div className="col-sm-6 text-right">
                                <p className="m-b-0">
                                  {/* <span className="f-w-600 f-16">Lihat Semua</span> */}
                                </p>
                              </div>
                            </div>
                            <div className="wrap" style={{ overflowX: 'auto', overflowY: 'scroll', height: '400px' }}>

                              <ChartVertical />

                            </div>
                          </Card.Body>
                        </Card>
                      </div>
                      <div className="col-sm-12">
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className={`col-sm-12 col-xl-6 ${this.state.dataEvent === 'Dashboard' ? '' : 'hidden'}`} style={{ paddingLeft: 0, paddingRight: 0 }}>
                      <div className="col-sm-12">
                        <Card>
                          <Card.Body className="responsive-card-400 ">
                            <div className="row">
                              <div className="col-sm-6">
                                <h3 className="f-w-900 f-18 fc-blue">
                                  Schedule today
                                </h3>
                              </div>
                              <div className="col-sm-6 text-right">
                                <p className="m-b-0">
                                  <Link to={""}>
                                    <span className=" f-12 fc-skyblue">See all</span>
                                  </Link>
                                </p>
                              </div>
                            </div>
                            <div style={{ marginTop: '10px' }}>
                              <JadwalHariIni />
                            </div>
                          </Card.Body>
                        </Card>
                      </div>


                    </div>

                    <div className={`col-sm-12 ${this.state.dataEvent === 'Dashboard' ? 'col-xl-6' : this.state.dataEvent === 'Files' ? '' : 'hidden'}`} style={{ paddingLeft: 0, paddingRight: 0 }}>
                      <div className="col-sm-12">
                        <Card>
                          <Card.Body className="responsive-card-400 ">
                            <div className="row">
                              <div className="col-sm-6">
                                <h3 className="f-w-900 f-18 fc-blue">
                                  Files
                                </h3>
                              </div>
                              {/* <div className="col-sm-6 text-right">
                                <p className="m-b-0">
                                  <Link to={""}>
                                    <span className=" f-12 fc-skyblue">See all</span>
                                  </Link>
                                </p>
                              </div> */}
                            </div>
                            <div style={{ marginTop: '10px' }}>
                              <Files />
                            </div>
                          </Card.Body>
                        </Card>
                      </div>
                    </div>

                    <div className={`col-sm-12 ${this.state.dataEvent === 'Dashboard' || this.state.dataEvent === 'Calender' ? '' : 'hidden'}`}>
                      <CalenderNew lists={kursusTerbaru} />
                    </div>
                  </div>





                </div>
              </div>
            </div>
          </div>
        </div>

      </div >
    );
  }
}

const mapStateToProps = state => {
  return state.user;
}

const mapDispatchToProps = dispatch => ({
  initUser: () => dispatch(initUser())
})

const HomeWithTranslate = withTranslation('common')(HomeNew)

const HomeNewProps = connect(mapStateToProps, mapDispatchToProps)(HomeWithTranslate);

class HomeV2 extends Component {

  // state = {
  //   level: Storage.get('user').level,
  //   grupId: Storage.get('user').data.grup_id,
  //   grupName: Storage.get('user').data.grup_name,
  //   training_company_id: ''
  // }

  componentDidMount() {

    // let level = Storage.get('user').level;
    // let grupName = Storage.get('user').data.grup_name;
    // if (level.toLowerCase() === 'client' && grupName.toLowerCase() === 'admin training') {
    //   API.get(`${API_SERVER}v2/training/user/read/user/${Storage.get('user').userId}`).then(res => {
    //     if (res.status === 200) {
    //       this.setState({ training_company_id: res.data.result.training_company_id });
    //     }
    //   })
    // }
  }

  goTo(url) {
    if (url === 'back') {
      this.props.history.goBack();
    }
    else {
      this.props.history.push(url);
    }
  }

  render() {
    return <HomeNewProps />
  }

}

export default HomeV2;
