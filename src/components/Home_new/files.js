import React, { Component } from "react";
import Storage from '../../repository/storage';
import API, { API_SERVER } from '../../repository/api';

import { bodyTabble } from '../../modul/data';
import moment from 'moment-timezone';
import { toast } from "react-toastify";

class Files extends Component {
  state = {
    userId: Storage.get('user').userId,
    dataFiles: [],
    toDo: '',
    header: [
      { title: 'File Name', width: null, status: true },
      { title: 'Description', width: null, status: true },
      { title: 'Extension', width: null, status: true },

    ]
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    API.get(`${API_SERVER}files`).then(res => {
      if (res.data.error) toast.warning("Gagal fetch API");
      this.setState({ dataFiles: res.data.result });
    });
  }

  render() {
    const headerTabble = this.state.header;

    return (
      <div className="row">
        <div className="table-responsive">
          <table className="table table-hover" style={{ whiteSpace: 'nowrap' }}>
            <thead>
              <tr style={{ borderBottom: '1px solid #C7C7C7' }}>
                {
                  headerTabble.map((item, i) => {
                    return (
                      <td align="left" width={item.width}><b>{item.title}</b></td>
                    )
                  })
                }
                <td colSpan="2" align="center"><b> Action </b></td>
              </tr>
            </thead>
            <tbody>
              {
                this.state.dataFiles.length === 0 ?
                  <tr>
                    <td className="fc-muted f-14 f-w-300 p-t-20" colspan='8'>There is no</td>
                  </tr>
                  :
                  this.state.dataFiles.map((item, i) => {
                    return (
                      <tr style={{ borderBottom: '1px solid #DDDDDD' }}>
                        <td className="fc-muted f-14 f-w-300 p-t-20" >{item.fileName}</td>
                        <td className="fc-muted f-14 f-w-300 p-t-20" align="center">{item.description}</td>
                        <td className="fc-muted f-14 f-w-300 p-t-20" align="center">{item.extension}</td>
                        <td className="fc-muted f-14 f-w-300" align="center">
                          <button className="btn btn-icademy-file" >
                            <i className="fa fa-download fc-skyblue"></i> Download File
                          </button>
                        </td>
                      </tr>
                    )
                  })
              }

            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Files;
