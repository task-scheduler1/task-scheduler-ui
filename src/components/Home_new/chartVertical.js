import React, { Component } from "react";

import { Bar } from 'react-chartjs-2';



export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top',
    },
    title: {
      display: true,
      text: 'Chart.js Bar Chart',
    },
  },
};

const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

export const data = {
  labels,
  datasets: [
    {
      label: 'Dataset 1',
      data: [65, 59, 80, 81, 56, 55, 40],
      backgroundColor: 'rgba(255, 99, 132, 0.5)',
    },
    {
      label: 'Dataset 2',
      data: [100, 30, 64, 22, 98, 90, 87],
      backgroundColor: 'rgba(53, 162, 235, 0.5)',
    },
  ],
};

class ChartVertical extends Component {
  state = {
    data: []
  }

  componentDidMount() {
    // this.fetchDataUser();
  }

  render() {
    return (
      <div >
        <Bar options={options} data={data} />
      </div>
    );
  }
}

export default ChartVertical;
