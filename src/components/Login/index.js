import React, { Component } from "react";
import { Alert } from 'react-bootstrap';
import axios from 'axios';
import API, { LOGIN, API_SERVER } from '../../repository/api';
import Storage from '../../repository/storage';
import LupaPassword from './lupaPassword'
// import { Link } from "react-router-dom";
import { isMobile, isIOS } from 'react-device-detect';

// const tabs = [
//   { title: 'Login' },
//   { title: 'Vouchers' },
// ];


class Login extends Component {
  // constructor(props) {
  //   super(props);
  // }

  state = {
    email: '',
    password: '',
    username: '',
    toggle_alert: false,
    isVoucher: false,
    voucher: '',
    alertMessage: '',
    tabIndex: 1,
    showPass: false,
    showOpenApps: true
  };

  tabLogin(e, a, b) {
    e.preventDefault()
    this.setState({ tabIndex: b + 1 });
    if (b === 1) {
      this.setState({ isVoucher: true, voucher: '', email: '', password: '', toggle_alert: false });
    } else {
      this.setState({ isVoucher: false, voucher: '', email: '', password: '', toggle_alert: false });
    }
    // console.log(b, this.state.tabIndex)
  }

  backToLogin() {
    //console.log('balikk')
    this.setState({ isVoucher: false, voucher: '', email: '', password: '', toggle_alert: false, showPass: false, tabIndex: 1 });
  }

  lupaPassword() {
    this.setState({ isVoucher: true, voucher: '', email: '', password: '', toggle_alert: true, showPass: true, tabIndex: 4 });
  }


  onChangeEmail = e => {
    this.setState({ email: e.target.value, toggle_alert: false });
  };

  onChangePassword = e => {
    this.setState({ password: e.target.value, toggle_alert: false, });
  };


  onClickEmail = e => {
    e.preventDefault();
    this.setState({ isVoucher: false, voucher: '', email: '', password: '', toggle_alert: false });
  };



  submitFormLogin = e => {
    e.preventDefault();
    let body = {

      email: this.state.email, password: this.state.password

    }

    API.post(`${API_SERVER}login`, body).then((res) => {
      if (!res.data.error) {
        Storage.set('user', { token: res.data.result.token, level: 0, userId: res.data.result.userId });
        window.location.href = window.location.origin;
      } else {
        this.setState({ toggle_alert: true, alertMessage: 'Login failed. Please verify the data correct!' });
      }
    });

  }
  componentDidMount() {
    try {
      if (this.props.match.params.id && this.props.match.params.key) {
        this.lupaPassword()
      }
    } catch (error) {
      console.log('Continue', error)
    }
  }
  render() {
    const { toggle_alert } = this.state;
    let plainURL = this.props.redirectUrl ? this.props.redirectUrl.slice(1, 20) === 'meeting/information' ? decodeURIComponent('https://' + 'redirect' + this.props.redirectUrl) : decodeURIComponent('https://' + this.props.redirectUrl.slice(1, this.props.redirectUrl.length)) : 'APPS_SERVER';
    let lengthURL = plainURL.length;
    let iosURL = 'icademy' + plainURL.slice(5, lengthURL)


    return (
      <div style={{ background: "#fafbfc" }}>
        <header className="header-login">
          <center>
            <div className="mb-4">
              <img
                src="newasset/task/logo-task.png"
                style={{ paddingTop: 18, width: '20vh' }}
                alt=""
              />
            </div>
          </center>
        </header>
        <div className="auth-wrapper">
          <div className="auth-content mb-4">
            <div className="card b-r-15">
              <div
                className="card-body text-center"
                style={{ padding: "50px !important" }}
              >
                <div className="row ">
                  <span className={!this.state.showPass ? 'hidden' : ''} style={{ color: '#00478C', paddingLeft: 15, cursor: 'pointer' }}
                    onClick={this.backToLogin.bind(this)}>
                    <i className="fa fa-arrow-left fa-2x"></i>
                  </span>

                  <div className={this.state.showPass ? 'hidden' : 'ml-4 mb-4'}>
                    <h3> Login </h3>
                  </div>

                  {this.state.tabIndex === 1 ? (
                    <div className="col-sm-12">
                      <form onSubmit={this.submitFormLogin}>
                        <b style={{ float: 'left', color: 'black' }}>Email</b>
                        <div className="input-group mb-4">
                          <input
                            type="text"
                            value={this.state.email}
                            className="form-control"
                            style={{ marginTop: 8 }}
                            placeholder="Enter your email"
                            onChange={this.onChangeEmail}
                            required
                          />
                        </div>
                        <b style={{ float: 'left', color: 'black' }}>Password</b>
                        <div className="input-group mb-3">
                          <input
                            type="password"
                            value={this.state.password}
                            className="form-control"
                            style={{ marginTop: 8 }}
                            placeholder="Enter your password"
                            onChange={this.onChangePassword}
                            required
                          />
                        </div>

                        <p className="mt-5">
                          <a href style={{ cursor: 'pointer', color: '#00478C' }} onClick={this.lupaPassword.bind(this)}>Forgot Password ?</a>
                        </p>
                        <button type="submit" className="btn btn-ideku col-12 shadow-2 mb-3 mt-4 b-r-3 f-16" style={{ height: 60 }}>
                          Login
                        </button>
                        {
                          toggle_alert &&
                          <Alert variant={'danger'}>
                            {this.state.alertMessage}
                          </Alert>
                        }
                      </form>
                    </div>) : (
                    <div><LupaPassword id={this.props.match.params.id} otp={this.props.match.params.key} /></div>
                  )}


                </div>

              </div>
            </div>
          </div>
          <div className="auth-content mb-4" style={{ display: isMobile ? 'none' : 'block' }}>
            <div className=" b-r-15">
              <div
                className=" text-center"
                style={{ padding: "50px !important" }}
              >
                <div className="mb-4">
                  <img
                    src="newasset/task/user-computer.png"
                    style={{ width: 550 }}
                    alt=""
                  />
                </div>
                <h4 className="mb-0 mt-1" style={{ textTransform: 'uppercase' }}>
                  <b>Connect with people anytime anywhere</b>
                </h4>
                <p className="mb-0 mt-1">
                  We are ready to connect you with others
                </p>

              </div>
            </div>
          </div>

        </div>

        {
          isMobile && this.state.showOpenApps ?
            <div className="floating-message">
              <button className="floating-close" onClick={() => this.setState({ showOpenApps: false })}><i className="fa fa-times"></i></button>
              <p style={{ marginTop: 8 }}>Want to use mobile apps ?</p>
              <a href={isIOS ? 'https://www.google.com' : 'https://www.google.com'}>
                <button className="button-flat-light"><i className="fa fa-download"></i> Install</button>
              </a>
              <a href={isIOS ? iosURL : plainURL}>
                <button className="button-flat-fill"><i className="fa fa-mobile-alt"></i> Open Apps</button>
              </a>
            </div>
            : null
        }
      </div>
    );
  }
}

export default Login;
