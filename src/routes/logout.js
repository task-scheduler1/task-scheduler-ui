import React from "react";

import API, { API_SERVER } from '../repository/api';
import Storage from '../repository/storage';
const axios = require('axios');

export default class Logout extends React.Component {
  constructor(props) {
    super(props);

    this.onClickLogout = this.onClickLogout.bind(this);
  }

  onClickLogout(e) {
    e.preventDefault();
  }

  componentDidMount() {
    const user_id = 'aBxLB';

    API.delete(`${API_SERVER}logout`, { data: { userId: user_id } }).then((res) => {
      if (!res.data.error) {
        localStorage.clear();
        window.location.href = window.location.origin;
      } else {
        console.log('GAGAL LOGOUT', res.data.error)
      }
    });
  }

  render() {
    return <div>Loading</div>;
  }
}