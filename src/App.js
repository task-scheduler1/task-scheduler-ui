import React, { lazy } from "react";
import './actions/i18n';

import { Switch, Route } from "react-router-dom";

import { API_SOCKET } from './repository/api';
import Storage from './repository/storage';

// import io from 'socket.io-client';

import Header from "./components/Header_sidebar/Header";
import Sidebar from "./components/Header_sidebar/Sidebar";
import Loader from "./components/Header_sidebar/Loader";

//gantt public
import Login from "./components/Login/index";

import ForgotPassword from './components/forgotPassword';
import OTP from './components/OTP';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'react-sm-select/dist/styles.css';

const SuperAdminSwitch = lazy(() => import("./routes/superadmin"));
const GanttPublic = lazy(() => import("./components/Gantt/GanttPublic"));
const ThankYou = lazy(() => import("./components/public/thankyou"));

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userLogin: false
    };
  }

  componentDidMount() {
    let userInfo = localStorage.getItem("user");
    if (userInfo === null) {
      this.setState({ userLogin: false });
    } else {
      this.setState({ userLogin: true });
    }
  }

  render() {
    let workSpace = null;
    if (this.state.userLogin) {
      workSpace = <Main />;
    } else {
      workSpace = <PublicContent />;
    }

    return (
      <div>{workSpace}</div>
    );
  }
}

export class PublicContent extends React.Component {
  render() {
    return (
      <div>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <Switch>
          <Route path="/" exact component={Login} />
          {/* <Route path="/meeting/:roomid" exact component={MeetingRoomPublic} /> */}
          {/* <Route path="/meet/:roomid" exact component={MeetRoomPub} /> */}
          <Route path="/gantt/:companyId/:projectId/:userId" exact component={GanttPublic} />
          {/* <Route path="/mobile-meeting/:url+" exact component={MobileMeeting} /> */}
          <Route path='/forgot-password' component={ForgotPassword} />
          <Route path='/OTP/:id' component={OTP} />
          <Route path='/reset-password/:id/:key' component={Login} />
          <Route path="/thankyou" exact component={ThankYou} />
          <Route component={Login} />
        </Switch>
      </div>
    );
  }
}



export class Main extends React.Component {
  state = {
    level: Storage.get('user').level
  }

  render() {
    let workSpaceSwitch = null;
    console.log('levell', this.state.level);
    if (this.state.level === 'superadmin') {
      workSpaceSwitch = <SuperAdminSwitch />;
    } else if (this.state.level === 'admin') {
      workSpaceSwitch = <SuperAdminSwitch />;

      // workSpaceSwitch = <AdminSwitch />;
    } else {
      workSpaceSwitch = <SuperAdminSwitch />;

      // workSpaceSwitch = <ClientSwitch />;
    }

    return (
      <div>
        <Loader />
        <Sidebar />
        <Header />
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        {workSpaceSwitch}
      </div>
    );
  }
}
